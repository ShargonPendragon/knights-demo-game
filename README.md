# Knights Demo Game
This is an example [Solarus Quest](https://www.solarus-games.org/) which uses the [Visual Novel System](https://gitlab.com/ShargonPendragon/visual-novel-system). Please see the [knights spreadsheet](knights_spreadsheet.csv) included in this project for what knights demo which Visual Novel System funtions
