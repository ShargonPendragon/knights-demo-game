local map_helper = {}

function map_helper:dialog_actions(map,name)
    map:remove_entities(name)
    map:get_game():set_value(name, true)
    map:get_game():save()
  end

function map_helper:start_actions(map,name)
  local entity_enabled = false
  if map:get_game():get_value(name) == nil then
    map:get_entity(name):set_enabled(true)
    entity_enabled = true
  end

  return entity_enabled

end

function map_helper:check_interacted_with_other_npc(map,check_string,start_num,end_num)
  local accept = true
  for i = start_num,end_num do
      if map:get_game():get_value(check_string..i) == nil then accept = false end
  end
  return accept
end

function map_helper:dynamic_tiles_enabled(map,enable)
  for entity in map:get_entities_by_type("dynamic_tile") do
    if enable then entity:set_enabled(true) else map:remove_entities(entity:get_name()) end
  end
end

return map_helper
