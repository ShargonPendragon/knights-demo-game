image = {
  path = "demo/visual_novel_art/characters/silver_knight/SilverKnightSingle.png",
  position = "right",
  x_offset = 0,
  y_offset = 0
}
dialog_box = {
  image = {
    position = "center",
    path = "demo/hud/dialog_boxes/dialog_box.png"
  }
}
emotions = {
  angry = {
    image = {
      path = "demo/visual_novel_art/characters/red_knight/RedKnight.png",
      position = "left",
      x_offset = 0,
      y_offset = 0
    }
  },
  envy = {
    image = {
      path = "demo/visual_novel_art/characters/green_knight/GreenKnight.png",
      position = "left",
      x_offset = 0,
      y_offset = 0
    }
  }
}