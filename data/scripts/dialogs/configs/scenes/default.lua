background = {
  image = {
    path = 'default'
  }
}
dialog_box = { -- contains ALL dialog box info (including name box)
  close_delay = 0, -- How much time do want to delay before closing the dialog box? (1000 = 1 second)
  image = {
    position = "center", -- The position the dialog box should display at. (can also be a table containing xy values)
    path = "demo/hud/dialog_boxes/dialog_box.png",
  },
  text = { -- text inside the dialog box
    max_displayed_lines = 4, -- The max number of lines to display at a time.
    x_offset = 8,
    y_offset = 14,
    line_space = 14, -- The space between each line
    line = { -- options for the indvidual lines.
      speed = "fast", -- how fast to display the lines. values are: slow medium fast, instant (you can also pass a number if you want a specific speed)
      horizontal_alignment = "left", --"left", "center" or "right".
      vertical_alignment = "middle", -- "top", "middle" or "bottom"
      --font = "Pacha",
      font = "ComicNeue-Angular-Bold",
      font_size = 12,
    },
    question = { -- config options that only apply when text contains a question
      line_buffer = 7, -- The amount of spaces to append to each question line (to leave room for the cursor)
      question_marker = "$?", -- If these markers appear at the beginning of 2 consecutive lines it's a question box
      cursor_wrap = true, -- Allow the cursor to wrap (i.e. if pressing up from top option move cursor to bottom option and vice versa)
      cursor = { -- config options for how the cursor should be displayed (either sprite or icon)
        image = {
          path = "dialog_test_art_assets/hud/cursor/icons/individual_icons/crystal_01c.png",  -- Please check
          --(scripts/dialogs/configs/scenes/examples/question_box/cursors/) for info on the different cursors you can use

        -- The cursor offsets exist because I can't know where you want your cursor in relation to the text. So I'm leaving it to you to fine tune it
          offset_x = 0, -- x offset from the start of the text surface (can be positive or negative number)
          offset_y = -8, -- y offset from the start of the text surface (can be positive or negative number)
        }
      },
    }
  },
  name_box = { -- name box information
    image = {
      path = "demo/hud/dialog_boxes/name_box.png",
      position = "outsidetopleft", -- The position the dialog box should display at. (can also be a table containing xy values)
    },
    line = { -- options for the indvidual lines.
      x_offset = 3, -- The offset so the text appears inside the dialog box border
      y_offset = 10, -- The offset for the first line from the top of the box
      horizontal_alignment = "left", --"left", "center" or "right".
      vertical_alignment = "middle", -- "top", "middle" or "bottom"
      font = "ComicNeue-Angular-Bold",
      font_size = 10,
    }
  }
}

