dialog_box = {
	name_box = {
		image = {
			path = "demo/hud/dialog_boxes/animated_name_box",
			position = "outsidetopleft",
			sprite = {
				animation = "default"
			}
		},
		line = {
			x_offset = -5,
			y_offset = -5,
			color = {255,0,255},
			font_size = 14
		}
	}
}

