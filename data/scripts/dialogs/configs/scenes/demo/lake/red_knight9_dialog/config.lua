dialog_box = {
  close_delay = 1000,
	text = {
    question = {
      move_sound = "dialog_test_sfx_assets/Checkout_Scanner_Beep",
      cursor = {
        image = {
					path = "demo/hud/cursor/sprites/ball_shrunk",
					y_offset = 5,
					x_offset = 15,
					sprite = {
						animation = "idle"
					}
				}
			},
      answer = {
        image = {
          path = "demo/hud/cursor/sprites/ball_shrunk",
          y_offset = 0,
          x_offset = 15,
          sprite = {
            animation = "confirmation"
          }
        }
      }
    }
  }
}
