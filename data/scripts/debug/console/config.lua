local console = {
  color = {0, 0, 225},  --  Background Color
  opacity = 150, -- Opacity
  --input_font = "SourceCodePro-ExtraLight", -- Input text font
  input_font_size = 12, -- Input text size
  --output_font = "SourceCodePro-ExtraLight", -- Output text font
  output_font_size = "12", -- Output text size
}

return console
