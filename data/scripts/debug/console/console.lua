-- The ; key toggles the debug console while the game is running.
local console = {
  width = 0, -- Screen Width
  height = 0, -- Screen Height
  color = {255, 255, 255},  -- Color and Opacity of the console
  opacity = 255, -- Opacity of the console
  history = { {} },        -- Array of commands entered into the console
  history_position = 1,    -- The position of the currently view command in history.
  input_text_surface = nil, -- The text surface where the user inputs commands.
  input_position = 0, -- Current position of the to input characters in the input field
  output_text = {
    lines = {sol.text_surface.create{}}, -- INTERNAL ONLY DO NOT SET! Array of text surfaces to be displayed
    text = {}, -- INTERNAL ONLY DO NOT SET! Array of output strings
    max_display_number = nil, -- INTERNAL ONLY DO NOT SET! Max number of output displays that can fit in the current console
    vertical_alignment = "bottom" -- INTERNAL ONLY DO NOT SET! Text serface alignment
  },
}

local PATH = ...
local top_dir = PATH:match"^.+/" or ""
local game_meta = sol.main.get_metatable("game")
local commands = {}
local config = nil

-- Loads in the table from the config script into the console table
local function load_config()
  local path = top_dir .. "config.lua"
  local data = package.loaded[path]

  if not data then
    config = require(path)
    for config_key, config_value in pairs(config) do
      console[config_key] = config_value
    end
  end
end

-- Set commands table to contain all the command scripts in the commands folder.
local function setup_commands()
  local command_folder_path = top_dir.."commands"
  local files = sol.file.list_dir(command_folder_path)

  for _, file_value in pairs(files) do
    local lua_file = file_value:find(".lua")
    if lua_file ~= nil then
      local path = command_folder_path .. "/" .. file_value
      local data = package.loaded[path]
      if not data then
        commands[file_value:sub(1, lua_file - 1)] = require(path)
      end
    end
  end
end

-- Creates the text surface the user will be entering commands on
local function create_input_text_serface()
  console.input_text_surface = sol.text_surface.create{
    font = console.input_font or nil,
    font_size = console.input_font_size or nil
  }
end

-- Creates the text serfaces that will display the command outputs
local function create_output_text_serfaces()
  local font = console.output_font or  console.output_text.lines[1]:get_font()
  local size = console.output_font_size or console.output_text.lines[1]:get_font_size()
  local _, height = sol.text_surface.get_predicted_size(font, size, "")

  console.output_text.max_display_number = math.floor(console.height / height / 2 - 1)
  table.remove (console.output_text.lines)
  for _ = 1, console.output_text.max_display_number, 1 do
    table.insert(console.output_text.lines,
      sol.text_surface.create{
        font = font,
        font_size = size,
        vertical_alignment = console.output_text.vertical_alignment
      }
    )
  end
end

-- Displays the user entered text in the console. Also handles placing cursor in correct spot in the entered text.
--
-- Example
--   cursor("Fred is Here", true)
--     => "Fred is Here|"
--
-- Returns nothing
local function display_input_text()
  local text = table.concat(console.history[console.history_position])
  text = string.sub(text, 1, console.input_position) .. "|" .. string.sub(text,console.input_position +1, string.len(text))
  console.input_text_surface:set_text(text)
end

-- On console start up, calls functions to set up console
function console:on_started()
  load_config()
  setup_commands()
  create_input_text_serface()
  display_input_text()
  table.insert(console.color, 4, console.opacity)
end

-- Displays the console output. Either the most recent or historical if the user pages up or down
local function display_output_text()
  local text_index = console.output_text.history_position
  for _, line_value in pairs(console.output_text.lines) do
    line_value:set_text(console.output_text.text[text_index])
    text_index = text_index - 1
  end
end

-- Add a new line of text to the output text table
--
-- text -- The text to be added to the output text table.(String)
--
-- Example
--   add_text_to_output_table("Output Text")
--
-- Returns nothing
local function add_text_to_output_table(text)
  table.insert(console.output_text.text, text)
  console.output_text.history_position = #console.output_text.text
  display_output_text()
  if console.output_text.max_display_number == nil then create_output_text_serfaces() end
end

-- This takes the output text splits it apart to fit in the console
--
-- output_text - Text needing to be added to the output.(String)
--
-- Example
--   set_output_text("Output Text")
--
-- Returns Nothing
local function set_output_text(output_text)

  local text_surface_width, _ =
    sol.text_surface.get_predicted_size(
      console.output_text.lines[1]:get_font(),
      console.output_text.lines[1]:get_font_size(),
      output_text
    )

  if text_surface_width > console.width then
    local character_width, _ = sol.text_surface.get_predicted_size(console.output_text.lines[1]:get_font(), console.output_text.lines[1]:get_font_size(), " ")
    local max_characters = math.floor(console.width / character_width)
    local output_text_lenght = string.len(output_text)

    for i = 1, output_text_lenght, max_characters do
      add_text_to_output_table(string.sub(output_text, i, i + max_characters - 1))
    end
  else
    add_text_to_output_table(output_text)
  end
end

-- This updates the input field table
--
-- character - character to be added to the input field.(String)
--
-- Example
--   update_input("A")
--
-- Return Nothing
local function update_input(character)
  local char = character or ""
  local input_text_table = console.history[console.history_position]

  if char ~= "" then
    console.input_position = console.input_position + 1
    table.insert(input_text_table,console.input_position, character)
  else
    table.remove(input_text_table,console.input_position)
    console.input_position = console.input_position - 1
  end
  display_input_text()
end

-- Adds the user entered command to the console history
local function add_command_to_history()
  console.history_position = #console.history + 1
  console.history[console.history_position] = {}
  console.input_position = 0
  display_input_text()
end

-- Used to determine how to break up output text
--
-- text - Raw date returned from a command
--
local function output_text(text)
  local var_type = type(text)
  if var_type == "string" then set_output_text(text)
  elseif var_type == "table" then
    for _, text_value in pairs(text) do set_output_text(text_value) end
  end
end

-- Execute the entered command in the input text surface.
local function execute_code()
  local input_text = table.concat(console.history[console.history_position])
  local input_text_table = {}
  for input_word in input_text:gmatch('[^ %s ]+') do table.insert(input_text_table, input_word) end
  add_command_to_history()

  for command_key, command_value in pairs(commands) do
    if command_key == input_text_table[1] then
      local returned_data = command_value:command(unpack(input_text_table,2))
      output_text(input_text)
      if returned_data ~= nil then output_text(returned_data) end
    end
  end
end

-- This shifts output text to display past output lines
--
-- shift - determines if we are shifting up or down. (String)
--
-- Example
--   shift_output_history("up")
--
-- Returns Nothing
local function shift_output_history(shift)
  shift = string.lower(shift)
  if shift == "up" and console.output_text.history_position - console.output_text.max_display_number > 0 then
    console.output_text.history_position = console.output_text.history_position - console.output_text.max_display_number
  elseif shift == "down" and console.output_text.history_position + console.output_text.max_display_number <= #console.output_text.text then
    console.output_text.history_position = console.output_text.history_position + console.output_text.max_display_number
  end

  display_output_text()
end

-- Shift history output history up one page
local function output_history_up()
  shift_output_history("up")
end

-- Shift history output history down one page
local function output_history_down()
  shift_output_history("down")
end

-- Moves the cursor position in the input field
--
-- shift - Value determining how far and witch direction to move the cursor. (Number)
--
-- Example
--   shift_cursor(-1)
--
-- Returns nothing
local function shift_cursor(shift)
  if console.input_position + shift >= 0 and console.input_position + shift <= #console.history[console.history_position] then
    console.input_position = console.input_position + shift
    display_input_text()
  end
end

-- Shift cursor to the left by one
local function shift_cursor_left()
  shift_cursor(-1)
end

-- Shift cursor to the right by one
local function shift_cursor_right()
  shift_cursor(1)
end

-- Shifts the input history by number
--
-- shift_value - How much to shift the input history by
--
-- Example
--   shift_history(1)
--
-- Returns nothing
local function shift_history(shift_value)
  if console.history_position + shift_value >= 1 and console.history_position + shift_value <= #console.history then
    console.history_position = console.history_position + shift_value
    display_input_text()
  end
end

-- Moves the history input up
local function history_up()
  shift_history(-1)
end

-- Moves the history input down
local function history_down()
  shift_history(1)
end

-- Called by Solarus with a key is pressed
--
-- key - name of the key that was pressed
-- modifiers - contains information on any modifiers that were pressed
--
-- Example
--   N/A
--
-- Returns true
function console:on_key_pressed(key, modifiers)
  local  key_pressed = {
    ["`"] = sol.menu.stop,
    ["backspace"] = update_input,
    ["up"] = history_up,
    ["down"] = history_down,
    ["return"] = execute_code,
    ["kp return"] = execute_code,
    ["page up"] = output_history_up,
    ["page down"] = output_history_down,
    ["left"] = shift_cursor_left,
    ["right"] = shift_cursor_right
  }
  local func = key_pressed[key]

  if (func) and not modifiers.alt and not modifiers.control then
    if key == "`" then
      func(self)
    else
      func()
    end
  end

  return true
end

-- Solarus functino called when character pressed and adds it to the input field unless it's "`"
--
-- character - character that was pressed
--
-- Example
--   N/A
--
-- Returns nothing
function console:on_character_pressed(character)
   if character:find("`") == nil then
    update_input(character)
    return true
  end

  return false
end

--  Sets the x and y of all output text surface. Also calls there draw function
--
-- Surface - Solarus surface
--
-- Example
--   output_text_display(Solarus Surface)
--
-- Returns Nothing
local function output_text_display(surface)
  local _, height = sol.text_surface.get_predicted_size(console.output_text.lines[1]:get_font(), console.output_text.lines[1]:get_font_size(), "")
  local index = 0
  for _, line_value in pairs(console.output_text.lines) do
      index = index + 1
      line_value:set_xy(5,  console.height - index * height)
      line_value:draw(surface)
  end
end

-- Solarus on draw function for the console.
function console:on_draw(dst_surface)
  if dst_surface ~= nil and dst_surface['get_size'] ~= nil then
    console.width, console.height = dst_surface:get_size()
    dst_surface:fill_color(console.color, 0, console.height/2, console.width, console.height/2)
    self.input_text_surface:draw(dst_surface, 5, console.height - self.input_text_surface:get_font_size() * 1.333 / 2 )
    output_text_display(dst_surface)
  end
end

-- Listener for when ` is pressed in game in order to start the console
game_meta:register_event("on_key_pressed", function(self, key, _)
  if key == "`" and sol.menu.is_started(console) == false then
    sol.menu.start(self, console)
    return true
  end
  return false
end)

return console
