local list = {}

-- Provides a list of all the files in the map folder
--
-- path - Is the file path that is being searched for files (String)
-- file - The list of files contained in a path (Table)
-- map_list - Is the list of all the files found under data/maps (Table)
--
-- Example
--   list:command("data/maps", {"test1.lua", "test1.data"}, {"data/maps/test1.lua", "data/maps/test1.data"})
--     => {"data/maps/test1.lua", "data/maps/test1.data"}
--
-- Returns table
function list:command(path, file, map_list)
  local path_list = path or "data/maps"
  local file_list = file or sol.file.list_dir("data/maps")
  local map = map_list or {}
  if next(file_list) == nil then
    return map
  end

  local current_path = path_list .. "/"

  for _, v in pairs(file_list) do
    local file_path = current_path..v
    if sol.file.is_dir(file_path) == true then
      list:command(file_path, sol.file.list_dir(file_path), map)
    else
      table.insert(map, file_path)
    end
  end

  return map
end

return list

