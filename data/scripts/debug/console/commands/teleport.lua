local teleport = {}

-- Command used to teleport hero to a map
--
-- map_id - The map the hero will be teleporting to (String)
-- destination - Name of the destination entity (String)
-- transition - Type of transition the teleport will use (String)
--
-- Example
--   teleport:command("stealth/test1", "south", "immediate")
--
-- Returns nothing
function teleport:command(map_id, destination, transition)
  local game = sol.main.game
  local dest = destination or nil
  local tran = transition or "immediate"

  if map_id ~= nil or map_id ~= "" then
    game:get_hero():teleport(map_id, dest, tran)
  end
end

return teleport
