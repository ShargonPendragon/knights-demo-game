-- Lua script of map demo_map/lake/north_east.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local red_knight_3 = map:get_entity("red_knight_3")

-- Events called when the hero interacts with the Red Knight NPC
function red_knight_3:on_interaction()
  game:start_dialog('demo.lake.red_knight3_dialog', function() map_helper:dialog_actions(map,'red_knight_3')end)
end
-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'red_knight_3')
end