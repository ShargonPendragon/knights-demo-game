-- Lua script of map demo_map/lake/east.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local red_knight_4 =  map:get_entity("red_knight_4")

local function check_center_open()
  local center_open = map_helper:check_interacted_with_other_npc(map,"red_knight_",1,9)
  if center_open then map_helper:dynamic_tiles_enabled(map,true) end
end

-- Function for actions to be taken after talking to the Red Knight
local function knight_dialog_actions(knight_name)
  map_helper:dialog_actions(map,knight_name)
  check_center_open()
end

-- Events called when the hero interacts with the Green Knight NPC
function red_knight_4:on_interaction()
  game:start_dialog('demo.lake.red_knight4_dialog',function() knight_dialog_actions('red_knight_4')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'red_knight_4')
  check_center_open()
end
