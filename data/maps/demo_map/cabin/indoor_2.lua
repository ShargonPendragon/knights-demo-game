-- Lua script of map demo_map/cabin/indoor_cabin_2.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")

local green_knight_3 = map:get_entity('green_knight_3')
local green_knight_4 = map:get_entity('green_knight_4')
local green_knight_5 = map:get_entity('green_knight_5')
local green_knight_6 = map:get_entity('green_knight_6')
local green_knight_7 = map:get_entity('green_knight_7')
local green_knight_8 = map:get_entity('green_knight_8')
local green_knight_9 = map:get_entity('green_knight_9')
local green_knight_10 = map:get_entity('green_knight_10')
local green_knight_11 = map:get_entity('green_knight_11')
local green_knight_12 = map:get_entity('green_knight_12')
local green_knight_13 = map:get_entity('green_knight_13')
local green_knight_14 = map:get_entity('green_knight_14')

-- Below are event called when the hero interacts with the Green Knight NPCs
function green_knight_3:on_interaction()
  game:start_dialog('demo.cabin.green_knight3_dialog', function () map_helper:dialog_actions(map,'green_knight_3')end)
end

function green_knight_4:on_interaction()
  game:start_dialog('demo.cabin.green_knight4_dialog',  function () map_helper:dialog_actions(map,'green_knight_4')end)
end

function green_knight_5:on_interaction()
  game:start_dialog('demo.cabin.green_knight5_dialog', function () map_helper:dialog_actions(map,'green_knight_5')end)
end

function green_knight_6:on_interaction()
  game:start_dialog('demo.cabin.green_knight6_dialog', function () map_helper:dialog_actions(map,'green_knight_6')end)
end

function green_knight_7:on_interaction()
  game:start_dialog('demo.cabin.green_knight7_dialog', function () map_helper:dialog_actions(map,'green_knight_7')end)
end

function green_knight_8:on_interaction()
  game:start_dialog('demo.cabin.green_knight8_dialog', function () map_helper:dialog_actions(map,'green_knight_8')end)
end

function green_knight_9:on_interaction()
  game:start_dialog('demo.cabin.green_knight9_dialog', function () map_helper:dialog_actions(map,'green_knight_9')end)
end

function green_knight_10:on_interaction()
  game:start_dialog('demo.cabin.green_knight10_dialog', function () map_helper:dialog_actions(map,'green_knight_10')end)
end

function green_knight_11:on_interaction()
  game:start_dialog('demo.cabin.green_knight11_dialog', function () map_helper:dialog_actions(map,'green_knight_11')end)
end

function green_knight_12:on_interaction()
  game:start_dialog('demo.cabin.green_knight12_dialog', function () map_helper:dialog_actions(map,'green_knight_12')end)
end

function green_knight_13:on_interaction()
  game:start_dialog('demo.cabin.green_knight13_dialog', function () map_helper:dialog_actions(map,'green_knight_13')end)
end

function green_knight_14:on_interaction()
  local accept = map_helper:check_interacted_with_other_npc(map,'green_knight_',3,13)

 if accept then game:start_dialog('demo.cabin.green_knight14_dialog', function () map_helper:dialog_actions(map,'green_knight_14')end)
    else game:start_dialog('demo.cabin.green_knight14_dialog_reject') end

end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  for knight_num = 3,14 do
    local knight_entity = ('green_knight_'..knight_num)
    map_helper:start_actions(map,knight_entity)
  end
end
