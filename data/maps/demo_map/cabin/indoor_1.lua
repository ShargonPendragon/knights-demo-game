-- Lua script of map demo_map/cabin/indoor_cabin_1.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local knight = map:get_entity("green_knight_2")
-- Event called when the hero interacts with the Green Knight NPC
function knight:on_interaction()
  game:start_dialog('demo.cabin.green_knight2_dialog', function() map_helper:dialog_actions(map,'green_knight_2')end)
 end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'green_knight_2')
end