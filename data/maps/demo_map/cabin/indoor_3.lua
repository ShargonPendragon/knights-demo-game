-- Lua script of map demo_map/cabin/indoor_3.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local knight = map:get_entity("green_knight_15")

-- Function for actions to be taken after talking to the Green Knight
local function knight_dialog_actions(knight_name)
  map_helper:dialog_actions(map,knight_name)
  map_helper:dynamic_tiles_enabled(map,false)
end

-- Event called when the hero interacts with the Green Knight NPC
function knight:on_interaction()
  game:start_dialog('demo.cabin.green_knight15_dialog', knight_dialog_actions('green_knight_15'))
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  local entity_enabled = map_helper:start_actions(map,'green_knight_15')
  if entity_enabled then map_helper:dynamic_tiles_enabled(map,true) end
end
