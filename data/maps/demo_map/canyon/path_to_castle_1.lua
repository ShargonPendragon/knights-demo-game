-- Lua script of map demo_map/canyon/path_to_castle_1.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local bronze_knight_17 = map:get_entity("bronze_knight_17")
local bronze_knight_18 = map:get_entity("bronze_knight_18")
local bronze_knight_19 = map:get_entity("bronze_knight_19")

function bronze_knight_17:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight17_dialog',function()map_helper:dialog_actions(map,'bronze_knight_17')end)
end

function bronze_knight_18:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight18_dialog',function()map_helper:dialog_actions(map,'bronze_knight_18')end)
end

function bronze_knight_19:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight19_dialog',function()map_helper:dialog_actions(map,'bronze_knight_19')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'bronze_knight_17')
  map_helper:start_actions(map,'bronze_knight_18')
  map_helper:start_actions(map,'bronze_knight_19')
end