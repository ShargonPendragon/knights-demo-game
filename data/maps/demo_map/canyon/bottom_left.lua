-- Lua script of map demo_map/canyon/bottom_left.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local bronze_knight_1 = map:get_entity("bronze_knight_1")
local bronze_knight_5 = map:get_entity("bronze_knight_5")
local bronze_knight_6 = map:get_entity("bronze_knight_6")

function bronze_knight_1:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight1_dialog',function()map_helper:dialog_actions(map,'bronze_knight_1')end)
end

function bronze_knight_5:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight5_dialog',function()map_helper:dialog_actions(map,'bronze_knight_5')end)
end

function bronze_knight_6:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight6_dialog',function()map_helper:dialog_actions(map,'bronze_knight_6')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'bronze_knight_1')
  map_helper:start_actions(map,'bronze_knight_5')
  map_helper:start_actions(map,'bronze_knight_6')
end

