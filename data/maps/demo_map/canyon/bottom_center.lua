-- Lua script of map demo_map/canyon/bottom_center.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local bronze_knight_2 = map:get_entity("bronze_knight_2")
local bronze_knight_7 = map:get_entity("bronze_knight_7")
local bronze_knight_8 = map:get_entity("bronze_knight_8")

function bronze_knight_2:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight2_dialog',function()map_helper:dialog_actions(map,'bronze_knight_2')end)
end

function bronze_knight_7:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight7_dialog',function()map_helper:dialog_actions(map,'bronze_knight_7')end)
end

function bronze_knight_8:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight8_dialog',function()map_helper:dialog_actions(map,'bronze_knight_8')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'bronze_knight_2')
  map_helper:start_actions(map,'bronze_knight_7')
  map_helper:start_actions(map,'bronze_knight_8')
end
