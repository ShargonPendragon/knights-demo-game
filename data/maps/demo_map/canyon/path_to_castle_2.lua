-- Lua script of map demo_map/canyon/path_to_castle_2.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local bronze_knight_20 = map:get_entity("bronze_knight_20")
local bronze_knight_21 = map:get_entity("bronze_knight_21")

local function check_open()
  local path_open = map_helper:check_interacted_with_other_npc(map,"bronze_knight_",17,21)
  if path_open then map_helper:dynamic_tiles_enabled(map,false)
  else map_helper:dynamic_tiles_enabled(map,true) end
  return path_open
end

function bronze_knight_20:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight20_dialog',function()map_helper:dialog_actions(map,'bronze_knight_20') check_open() end)
end

function bronze_knight_21:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight21_dialog',function()map_helper:dialog_actions(map,'bronze_knight_21') check_open() end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  check_open()
  map_helper:start_actions(map,'bronze_knight_20')
  map_helper:start_actions(map,'bronze_knight_21')
end
