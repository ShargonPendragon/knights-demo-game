-- Lua script of map demo_map/canyon/top_center.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local bronze_knight_11 = map:get_entity("bronze_knight_11")
local bronze_knight_12 = map:get_entity("bronze_knight_12")

function bronze_knight_11:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight11_dialog',function()map_helper:dialog_actions(map,'bronze_knight_11')end)
end

function bronze_knight_12:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight12_dialog',function()map_helper:dialog_actions(map,'bronze_knight_12')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'bronze_knight_11')
  map_helper:start_actions(map,'bronze_knight_12')
end