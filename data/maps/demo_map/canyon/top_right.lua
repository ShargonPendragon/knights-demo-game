-- Lua script of map demo_map/canyon/top_right.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local bronze_knight_9 = map:get_entity("bronze_knight_9")
local bronze_knight_10 = map:get_entity("bronze_knight_10")
local bronze_knight_14 = map:get_entity("bronze_knight_14")

function bronze_knight_9:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight9_dialog',function()map_helper:dialog_actions(map,'bronze_knight_9')end)
end

function bronze_knight_10:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight10_dialog',function()map_helper:dialog_actions(map,'bronze_knight_10')end)
end

function bronze_knight_14:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight14_dialog',function()map_helper:dialog_actions(map,'bronze_knight_14')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'bronze_knight_9')
  map_helper:start_actions(map,'bronze_knight_10')
  map_helper:start_actions(map,'bronze_knight_14')

end