-- Lua script of map demo_map/canyon/top_left.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local bronze_knight_13 = map:get_entity("bronze_knight_13")
local bronze_knight_15 = map:get_entity("bronze_knight_15")
local bronze_knight_16 = map:get_entity("bronze_knight_16")

local function check_open()
  local path_open = map_helper:check_interacted_with_other_npc(map,"bronze_knight_",1,16)
  if path_open then map_helper:dynamic_tiles_enabled(map,false)
  else map_helper:dynamic_tiles_enabled(map,true) end
  return path_open
end

function bronze_knight_13:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight13_dialog',function()map_helper:dialog_actions(map,'bronze_knight_13')end)
end

function bronze_knight_15:on_interaction()
  game:start_dialog('demo.canyon.bronze_knight15_dialog',function()map_helper:dialog_actions(map,'bronze_knight_15')end)
end

function bronze_knight_16:on_interaction()
  local inter_other_npc = map_helper:check_interacted_with_other_npc(map,"bronze_knight_",1,15)
  if inter_other_npc then game:start_dialog('demo.canyon.bronze_knight16_dialog',function()map_helper:dialog_actions(map,'bronze_knight_16') check_open()end)
  else game:start_dialog('demo.canyon.bronze_knight16_dialog_reject') end
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  check_open()
  map_helper:start_actions(map,'bronze_knight_13')
  map_helper:start_actions(map,'bronze_knight_15')
  map_helper:start_actions(map,'bronze_knight_16')
end
