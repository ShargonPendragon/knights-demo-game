-- Lua script of map demo_map/forest/forest2.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local blue_knight_7 = map:get_entity("blue_knight_7")

-- Events called when the hero interacts with the Blue Knight NPC
function blue_knight_7:on_interaction()
  game:start_dialog('demo.forest.blue_knight7_dialog', function () map_helper:dialog_actions(map,'blue_knight_7') end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'blue_knight_7')
end
