-- Lua script of map demo_map/forest/forest_upper_center.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local blue_knight_8 = map:get_entity("blue_knight_8")
local blue_knight_4 = map:get_entity("blue_knight_4")

local function blue_knight_8_dialog_actions()
  map_helper:dialog_actions(map,'blue_knight_8')
  map_helper:dynamic_tiles_enabled(map,false)
end

function blue_knight_8:on_interaction()
local accept = map_helper:check_interacted_with_other_npc(map,'blue_knight_',2,7)

if accept then game:start_dialog('demo.forest.blue_knight8_dialog', function() blue_knight_8_dialog_actions()end)
   else game:start_dialog('demo.forest.blue_knight8_dialog_reject') end
end

-- Events called when the hero interacts with the Blue Knight NPC
function blue_knight_4:on_interaction()
  game:start_dialog('demo.forest.blue_knight4_dialog', function () map_helper:dialog_actions(map,'blue_knight_4') end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()

map_helper:start_actions(map,'blue_knight_4')

local knight_enabled = map_helper:start_actions(map,'blue_knight_8',true)
if knight_enabled then map_helper:dynamic_tiles_enabled(map,true) end

end