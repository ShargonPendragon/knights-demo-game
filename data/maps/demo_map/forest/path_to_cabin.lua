-- Lua script of map demo_map/forest/forest_path_to_cabin.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()
local map_helper = require("scripts/maps/map_helper")
local blue_knight_9 = map:get_entity("blue_knight_9")

-- Event called on when the hero interacts with the NPC
function blue_knight_9:on_interaction()
  game:start_dialog('demo.forest.blue_knight9_dialog', function() map_helper:dialog_actions(map,'blue_knight_9')
  map_helper:dynamic_tiles_enabled(map,false) end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  local knight_enabled = map_helper:start_actions(map,'blue_knight_9',true)
  if knight_enabled then map_helper:dynamic_tiles_enabled(map,true) end
end