-- Lua script of map demo_map/castle/town_right.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local silver_knight_9 = map:get_entity("silver_knight_9")
local silver_knight_10 = map:get_entity("silver_knight_10")
local silver_knight_11 = map:get_entity("silver_knight_11")
local silver_knight_12 = map:get_entity("silver_knight_12")

function silver_knight_9:on_interaction()
  game:start_dialog('demo.castle.silver_knight9_dialog',function()map_helper:dialog_actions(map,'silver_knight_9')end)
end

function silver_knight_10:on_interaction()
  game:start_dialog('demo.castle.silver_knight10_dialog',function()map_helper:dialog_actions(map,'silver_knight_10')end)
end

function silver_knight_11:on_interaction()
  game:start_dialog('demo.castle.silver_knight11_dialog',function()map_helper:dialog_actions(map,'silver_knight_11')end)
end
function silver_knight_12:on_interaction()
  game:start_dialog('demo.castle.silver_knight12_dialog',function()map_helper:dialog_actions(map,'silver_knight_12')end)
end
-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'silver_knight_9')
  map_helper:start_actions(map,'silver_knight_10')
  map_helper:start_actions(map,'silver_knight_11')
  map_helper:start_actions(map,'silver_knight_12')
end
