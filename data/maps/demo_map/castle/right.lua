-- Lua script of map demo_map/castle/town_top.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local silver_knight_7 = map:get_entity("silver_knight_7")
local silver_knight_8 = map:get_entity("silver_knight_8")

function silver_knight_7:on_interaction()
  game:start_dialog('demo.castle.silver_knight7_dialog',function()map_helper:dialog_actions(map,'silver_knight_7')end)
end

function silver_knight_8:on_interaction()
  game:start_dialog('demo.castle.silver_knight8_dialog',function()map_helper:dialog_actions(map,'silver_knight_8')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'silver_knight_7')
  map_helper:start_actions(map,'silver_knight_8')
end
