-- Lua script of map demo_map/castle/town_center.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()
local map_helper = require("scripts/maps/map_helper")
local silver_knight_3 = map:get_entity("silver_knight_3")
local silver_knight_4 = map:get_entity("silver_knight_4")
local silver_knight_13 = map:get_entity("silver_knight_13")

function silver_knight_3:on_interaction()
  game:start_dialog('demo.castle.silver_knight3_dialog',function()map_helper:dialog_actions(map,'silver_knight_3')end)
end

function silver_knight_4:on_interaction()
  game:start_dialog('demo.castle.silver_knight4_dialog',function()map_helper:dialog_actions(map,'silver_knight_4')end)
end

function silver_knight_13:on_interaction()
  game:start_dialog('demo.castle.silver_knight13_dialog',function()map_helper:dialog_actions(map,'silver_knight_13')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'silver_knight_3')
  map_helper:start_actions(map,'silver_knight_4')
end
