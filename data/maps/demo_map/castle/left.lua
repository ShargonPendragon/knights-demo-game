-- Lua script of map demo_map/castle/town_left.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local silver_knight_5 = map:get_entity("silver_knight_5")
local silver_knight_6 = map:get_entity("silver_knight_6")

function silver_knight_5:on_interaction()
  game:start_dialog('demo.castle.silver_knight5_dialog',function()map_helper:dialog_actions(map,'silver_knight_5')end)
end

function silver_knight_6:on_interaction()
  game:start_dialog('demo.castle.silver_knight6_dialog',function()map_helper:dialog_actions(map,'silver_knight_6')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'silver_knight_5')
  map_helper:start_actions(map,'silver_knight_6')
end