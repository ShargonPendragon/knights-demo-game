-- Lua script of map demo_map/desert/top_left.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local purple_knight_4 = map:get_entity("purple_knight_4")
local purple_knight_5 = map:get_entity("purple_knight_5")

local function check_open()
  local path_open = map_helper:check_interacted_with_other_npc(map,"purple_knight_",1,7)
  if path_open then map_helper:dynamic_tiles_enabled(map,false)
  else map_helper:dynamic_tiles_enabled(map,true) end
end

local function knight_dialog_actions(knight_name)
  map_helper:dialog_actions(map,knight_name)
  check_open()
end

function purple_knight_4:on_interaction()
  game:start_dialog('demo.desert.purple_knight4_dialog', function () knight_dialog_actions('purple_knight_4')end)
end

function purple_knight_5:on_interaction()
  game:start_dialog('demo.desert.purple_knight5_dialog', function () knight_dialog_actions('purple_knight_5')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  check_open()
  map_helper:start_actions(map,'purple_knight_4')
  map_helper:start_actions(map,'purple_knight_5')
end