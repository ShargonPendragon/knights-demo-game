-- Lua script of map demo_map/desert/bottom_right.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local purple_knight_2 = map:get_entity("purple_knight_2")
local purple_knight_3 = map:get_entity("purple_knight_3")

function purple_knight_2:on_interaction()
  game:start_dialog('demo.desert.purple_knight2_dialog', function () map_helper:dialog_actions(map,'purple_knight_2')end)
end

function purple_knight_3:on_interaction()
  game:start_dialog('demo.desert.purple_knight3_dialog', function () map_helper:dialog_actions(map,'purple_knight_3')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'purple_knight_2')
  map_helper:start_actions(map,'purple_knight_3')
end
