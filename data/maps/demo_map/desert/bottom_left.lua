-- Lua script of map demo_map/desert/bottom_left.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

local map_helper = require("scripts/maps/map_helper")
local purple_knight_1 = map:get_entity("purple_knight_1")

function purple_knight_1:on_interaction()
  game:start_dialog('demo.desert.purple_knight1_dialog', function () map_helper:dialog_actions(map,'purple_knight_1')end)
end

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()
  map_helper:start_actions(map,'purple_knight_1')

end