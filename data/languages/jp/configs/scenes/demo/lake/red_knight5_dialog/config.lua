dialog_box = {
	close_delay = 1000,
	text = {
		question = {
			cursor = {
				image = {
					path = "demo/hud/cursor/sprites/ball_shrunk",
					y_offset = 5,
					x_offset = 15,
					sprite = {
						animation = "idle"
					}
				}
			},
			answer = {
				image = {
					path = "demo/hud/cursor/icons/spritesheet.png",
					icon = {
						width = 16,
						height = 16,
						row = 5,
						column = 2
					}
				}
			}
		}
	}
}

