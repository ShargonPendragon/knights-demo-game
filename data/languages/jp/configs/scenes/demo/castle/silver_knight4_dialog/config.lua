characters = {
	silver_knight_3 = {
		image = {
			path = "demo/visual_novel_art/characters/silver_knight/silver_knight",
			position = "right",
			sprite = {
				animation = "idle",
				direction = 0
			}
		},
		dialog_box = {
       image = {
         position = "center",
         path = "demo/hud/dialog_boxes/dialog_box.png"
       }
     }
	}
}
