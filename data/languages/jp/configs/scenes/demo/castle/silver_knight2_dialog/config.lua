characters = {
	silver_knight = {
		image = {
			path = "demo/visual_novel_art/characters/portaits/SilverKnightSpritePortrait",
			position = "outsideright",
			relative_to_dialog_box = true,
			x_offset = 0,
			y_offset = 0,
			sprite = {
				animation = "idle",
				direction = 0
			}
		},
		dialog_box = {
       image = {
         position = "left",
         path = "demo/hud/dialog_boxes/smaller_dialog_box.png"
       }
     }
	}
}
